# README #

This README  contains information about using wondershaper for traffic shaping.

### What is this repository for? ###

* Quick summary
This repository is for the wondershaper tool v1.1a which will change the bandwidth at the interface.
* Version
Wondershaper v1.1a


### How do I get set up? ###

* Summary of set up

First we have installed the wondershaper in ubuntu v16.O4 LTS at server side. Then we have tested the original link capacity before changing the bandwidth with the wondershaper tool.

* Configuration

apt-get install wondershaper // to install wondershaper (not in root directory)

or

sudo apt-get install wondershaper // to install wondershaper (for root directory)

ifconfig // to check the running interface

sudo wondershaper enp0s3 200 128 // with this command we are changing downlink capacity to 200 Kbps for interface enp0s3.

sudo wondershaper enp0s3 500 128 // with this command we are changing downlink capacity to 500 Kbps for interface enp0s3.

sudo wondershaper enp0s3 750 128 // with this command we are changing downlink capacity to 750 Kbps for interface enp0s3.

sudo wondershaper enp0s3 1024 128 // with this command we are changing downlink capacity to 1024 Kbps for interface enp0s3.

sudo wondershaper enp0s3 2048 128 // with this command we are changing downlink capacity to 2048 Kbps for interface enp0s3.

sudo wondershaper clear enp0s3 // to clear the limit of bandwidth from interface enp0s3

### Who do I talk to? ###

Ankita Chauhan
